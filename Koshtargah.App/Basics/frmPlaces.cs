﻿using Koshtargah.DataLayer;
using Koshtargah.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class frmPlaces : Form
    {
        public frmPlaces()
        {
            InitializeComponent();
        }

        private void frmPlaces_Load(object sender, EventArgs e)
        {
            BindGrid();

            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            btnSave.Enabled = false;
        }

        void BindGrid()
        {
            using (UnitofWork db = new UnitofWork())
            {
                dgVPlaces.DataSource = db.KoshtargahRepository.GetAllPlaces();
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlAdd.Enabled = true;
            btnSave.Enabled = true;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            txtCode.Text = "";
            txtTitle.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text != "" && txtCode.Text != "" && txtTitle.Text != "")
                {
                    using (UnitofWork db = new UnitofWork())
                    {
                        tbl_Places places = new tbl_Places()
                        {
                            Code = int.Parse(txtCode.Text),
                            Title = txtTitle.Text,
                            Description = txtDescription.Text,
                            
                        };
                        db.KoshtargahRepository.InsertPlaces(places);
                        db.Save();
                        BindGrid();
                    }
                }
                //After Save
                btnSave.Enabled = false;
                txtCode.Text = "";
                txtTitle.Text = "";
                pnlAdd.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("خطا" + ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (UnitofWork db = new UnitofWork())
            {
                tbl_Places places = new tbl_Places()
                {
                    Code = int.Parse(txtCode.Text),
                    Title = txtTitle.Text,
                    Description = txtDescription.Text,

                };
                db.KoshtargahRepository.UpdatePlaces(places);
                db.Save();
                BindGrid();
            }

            //after save
            btnEdit.Enabled = false;
            txtCode.Text = "";
            txtTitle.Text = "";
            pnlAdd.Enabled = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgVPlaces.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgVPlaces.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف {name} مطمئن هستید ؟", "توجه", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int placesId = int.Parse(dgVPlaces.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeletePlaces(placesId);
                        db.Save();
                        BindGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("لطفا یکی از سطرهای بیماری را انتخاب نمایید");
            }
        }

        private void dgVPlaces_MouseClick(object sender, MouseEventArgs e)
        {
            btnDel.Enabled = true;
            btnEdit.Enabled = true;
        }

        private void dgVPlaces_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgVPlaces.CurrentRow != null)
                {
                    txtCode.Text = dgVPlaces.CurrentRow.Cells[0].Value.ToString();
                    txtTitle.Text = dgVPlaces.CurrentRow.Cells[1].Value.ToString();
                    
                    if (dgVPlaces.CurrentRow.Cells[2].Value != null)
                        txtDescription.Text = dgVPlaces.CurrentRow.Cells[2].Value.ToString();
                    

                    pnlAdd.Enabled = true;
                    btnEdit.Enabled = true;

                }
            }
            catch
            {
                MessageBox.Show("خطا در لود اطلاعات");
            }
        }

        private void dgVPlaces_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnSave_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    btnNew_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F8:
                    btnEdit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Delete:
                    btnDel_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;

            }
        }
    }
}
