﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Koshtargah.DataLayer;
using Koshtargah.DataLayer.Context;
using Koshtargah.DataLayer.Repositories;
using Koshtargah.DataLayer.Services;
using ValidationComponents;

namespace Koshtargah.App
{
    public partial class frmDamdar : Form
    {
        UnitofWork db = new UnitofWork();
        public frmDamdar()
        {
            InitializeComponent();
        }

        

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(BaseValidator.IsFormValid(this.components))
            {
                tbl_Damdar damdar = new tbl_Damdar()
                {
                    Name = txtName.Text,
                    Family = txtFamily.Text,
                    Father = txtFather.Text,
                    BirthDate = txtDateB.Text,
                    BirthPlace = txtBPlace.Text,
                    Identifier = txtIdentifier.Text,
                    Tel = txtTel.Text,
                    Mobile = txtMobile.Text,
                    Address = txtAddress.Text,
                    NationalCode = txtNC.Text,
                    Code_Tafsili = txtTafsili1.Text,

                };
                db.KoshtargahRepository.InsertDamdar(damdar);
                db.Save();
                pnlDamdar.Enabled = false;
                txtBD.Text = "";
                txtBPlace.Text = "";
                txtDateB.Text = "";
                txtDSodor.Text = "";
                txtFamily.Text = "";
                txtFather.Text = "";
                txtIdentifier.Text = "";
                txtName.Text = "";
                txtNC.Text = "";
                //txtSearch.Text = "";
                txtSerial.Text = "";
                txtTafsili1.Text = "";
                txtTafsili2.Text = "";
                btnSave.Enabled = false;
                BindGrid();
            }


        }

        private void M_Damdar_Load(object sender, EventArgs e)
        {
            pnlDamdar.Enabled = false;
            btnEdit.Enabled = false;
           // btnDel.Enabled = false;
            btnSave.Enabled = false;

            UnitofWork db = new UnitofWork();
            BindGrid();
           

        }
        void BindGrid()
        {
            using (db)
            {
                dgDamdar.AutoGenerateColumns = false;
                dgDamdar.DataSource = db.KoshtargahRepository.GetAllDamdar();
                //db.Dispose();
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlDamdar.Enabled = true;
            btnSave.Enabled = true;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            pnlDamdar.Enabled = false;
            txtBD.Text = "";
            txtBPlace.Text = "";
            txtDateB.Text = "";
            txtDSodor.Text = "";
            txtFamily.Text = "";
            txtFather.Text = "";
            txtIdentifier.Text = "";
            txtName.Text = "";
            txtNC.Text = "";
            //txtSearch.Text = "";
            txtSerial.Text = "";
            txtTafsili1.Text = "";
            txtTafsili2.Text = "";
            btnSave.Enabled = false;
            BindGrid();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            using (UnitofWork db = new UnitofWork())
            {
                dgDamdar.DataSource = db.KoshtargahRepository.GetDamdarByFilter(txtSearch.Text);
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgDamdar.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgDamdar.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف رکورد {name} ","توجه",MessageBoxButtons.YesNo
                        , MessageBoxIcon.Warning)==DialogResult.Yes)
                    {
                        int damdarCode = int.Parse(dgDamdar.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteDamdar(damdarCode);
                        db.Save();
                        BindGrid();
                    }
                }
            }
            else
            {
                RtlMessageBox .Show("ابتدا رکورد مورد نظر را انتخاب کنید");
            }
        }
    }
}
