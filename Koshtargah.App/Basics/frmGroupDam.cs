﻿using Koshtargah.DataLayer;
using Koshtargah.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class frmGroupDam : Form
    {
        public frmGroupDam()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlAdd.Enabled = true;
            btnSave.Enabled = true;
            txtCode.Text = "";
            txtTitle.Text = "";
            txtWeightAb.Text = "0";
            txtWeightChan.Text = "0";
            txtWeightGar.Text = "0";
            cmbKhat.Text = "";
            cmbNumbering.Text = "";
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
        }

        void BindGrid()
        {
            using (UnitofWork db = new UnitofWork())
            {
                dgVGroupDam.DataSource = db.KoshtargahRepository.GetAllGroupDam();
                //cmbKhat.DisplayMember = db.KoshtargahRepository.GetAllLineKoshtar().ToString();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text != "" && txtTitle.Text != "" )
                {
                    using (UnitofWork db = new UnitofWork())
                    {
                        tbl_GroupDam groupdam = new tbl_GroupDam()
                        {
                            Code = int.Parse(txtCode.Text),
                            Title = txtTitle.Text,
                            weightAb= int.Parse(txtWeightAb.Text),
                            weightChan= int.Parse(txtWeightChan.Text),
                            weightGar= int.Parse(txtWeightGar.Text),
                            Line= int.Parse(cmbKhat.Text),
                            Type_Numbering=cmbNumbering.Text
                            
                        };
                        db.KoshtargahRepository.InsertGroupDam(groupdam);
                        db.Save();
                        BindGrid();
                    }
                }

                //After Save
                btnSave.Enabled = false;
                txtCode.Text = "";
                txtTitle.Text = "";
                txtWeightAb.Text = "0";
                txtWeightChan.Text = "0";
                txtWeightGar.Text = "0";
                cmbKhat.Text = "";
                cmbNumbering.Text = "";
                pnlAdd.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("خطا" + ex.Message);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (UnitofWork db = new UnitofWork())
            {
                tbl_GroupDam groupdam = new tbl_GroupDam()
                {
                    Code = int.Parse(txtCode.Text),
                    Title = txtTitle.Text,
                    weightAb = int.Parse(txtWeightAb.Text),
                    weightChan = int.Parse(txtWeightChan.Text),
                    weightGar = int.Parse(txtWeightGar.Text),
                    Line = int.Parse(cmbKhat.Text),
                    Type_Numbering = cmbNumbering.Text

                };
                db.KoshtargahRepository.UpdateGroupDam(groupdam);
                db.Save();
                BindGrid();
            }

            //after save
            btnEdit.Enabled = false;
            txtCode.Text = "";
            txtTitle.Text = "";
            txtWeightAb.Text = "0";
            txtWeightChan.Text = "0";
            txtWeightGar.Text = "0";
            cmbKhat.Text = "";
            cmbNumbering.Text = "";
            pnlAdd.Enabled = false;
        }

        private void frmGroupDam_Load(object sender, EventArgs e)
        {
            BindGrid();

            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            btnSave.Enabled = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgVGroupDam.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgVGroupDam.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف {name} مطمئن هستید ؟", "توجه", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int groupdamId = int.Parse(dgVGroupDam.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteGroupDam(groupdamId);
                        db.Save();
                        BindGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("لطفا یکی از سطرهای گروه دام را انتخاب نمایید");
            }
        }

        private void dgVGroupDam_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgVGroupDam.CurrentRow != null)
                {
                    txtCode.Text = dgVGroupDam.CurrentRow.Cells[0].Value.ToString();
                    txtTitle.Text = dgVGroupDam.CurrentRow.Cells[1].Value.ToString();
                    cmbKhat.Text = dgVGroupDam.CurrentRow.Cells[2].Value.ToString();
                    cmbNumbering.Text = dgVGroupDam.CurrentRow.Cells[2].Value.ToString();
                    if (dgVGroupDam.CurrentRow.Cells[2].Value != null)
                        txtWeightAb.Text = dgVGroupDam.CurrentRow.Cells[3].Value.ToString();
                    if (dgVGroupDam.CurrentRow.Cells[3].Value != null)
                        txtWeightChan.Text = dgVGroupDam.CurrentRow.Cells[4].Value.ToString();
                    if (dgVGroupDam.CurrentRow.Cells[4].Value != null)
                        txtWeightGar.Text = dgVGroupDam.CurrentRow.Cells[5].Value.ToString();

                    pnlAdd.Enabled = true;
                    btnEdit.Enabled = true;

                }
            }
            catch
            {
                MessageBox.Show("خطا در لود اطلاعات");
            }
        }

        private void dgVGroupDam_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnSave_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    btnNew_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F8:
                    btnEdit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Delete:
                    btnDel_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;

            }
        }

        private void dgVGroupDam_MouseClick(object sender, MouseEventArgs e)
        {
            btnDel.Enabled = true;
            btnEdit.Enabled = true;
        }
    }
}
