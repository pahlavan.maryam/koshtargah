﻿using Koshtargah.DataLayer.Context;
using Koshtargah.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class frmBimari : Form
    {
        UnitofWork db = new UnitofWork();
        public frmBimari()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlAdd.Enabled = true;
            btnSave.Enabled = true;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            txtCode.Text = "";
            txtTitle.Text = "";
            txtDescription.Text = "";
        }

        private void frmBimari_Load(object sender, EventArgs e)
        {
            BindGrid();

            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            btnSave.Enabled = false;
            this.BringToFront();
            this.Focus();
            this.KeyPreview = true;
            //this.KeyPress += new KeyPressEventHandler(frmBimari_KeyPress);
        }

        void BindGrid()
        {
            using (db)
            {
                dgBimari.DataSource = db.KoshtargahRepository.GetAllBimari();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtCode.Text != "" && txtTitle.Text!="")
            {

               
            }
            //after save
            txtCode.Text = "";
            txtTitle.Text = "";
            txtDescription.Text = "";
            pnlAdd.Enabled = false;
            btnSave.Enabled = false;
            //MessageBox.Show();

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            tbl_Bimari bimari = new tbl_Bimari()
            {
                Code = int.Parse(txtCode.Text),
                Title = txtTitle.Text,
                Description = txtDescription.Text
            };
            db.KoshtargahRepository.UpdateBimari(bimari);
            db.Save();
            BindGrid();

            //after edit
            txtCode.Text = "";
            txtTitle.Text = "";
            txtDescription.Text = "";
            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgBimari.CurrentRow != null)
            {
                using (db)
                {
                    string name = dgBimari.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف {name} مطمئن هستید ؟", "توجه", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int bimariId = int.Parse(dgBimari.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteBimari(bimariId);
                        db.Save();
                        BindGrid();
                    }
                        
                }
            }
            else
            {
                MessageBox.Show("لطفا یکی از سطرهای بیماری را انتخاب نمایید");
            }
        }

        private void frmBimari_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show("Form.KeyPress: '" + e.KeyCode.ToString() + "' pressed.");
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnSave_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    btnNew_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F8:
                    btnEdit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Delete:
                    btnDel_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;

            }
        }

        private void dgBimari_MouseClick(object sender, MouseEventArgs e)
        {
            btnDel.Enabled = true;
            btnEdit.Enabled = true;
        }

        private void dgBimari_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgBimari.CurrentRow != null)
                {
                    txtCode.Text = dgBimari.CurrentRow.Cells[0].Value.ToString();
                    txtTitle.Text = dgBimari.CurrentRow.Cells[1].Value.ToString();
                    if (dgBimari.CurrentRow.Cells[2].Value != null)
                        txtDescription.Text = dgBimari.CurrentRow.Cells[2].Value.ToString();
                    else
                        txtDescription.Text = "";
                    pnlAdd.Enabled = true;
                    btnEdit.Enabled = true;
                    
                }
            }
            catch
            {
                MessageBox.Show("خطا در لود اطلاعات");
            }
            
        }
    }
}
