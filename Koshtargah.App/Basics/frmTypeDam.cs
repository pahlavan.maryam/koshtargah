﻿using Koshtargah.DataLayer;
using Koshtargah.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class frmTypeDam : Form
    {
        public frmTypeDam()
        {
            InitializeComponent();
        }

        void BindGrid()
        {
            using (UnitofWork db = new UnitofWork())
            {
                dgVTypeDam.DataSource = db.KoshtargahRepository.GetAllTypeDamdar();
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlAdd.Enabled = true;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            cmbGroup.Text = "";
            cmbSex.Text = "";
            txtCode.Text = "";
            txtTitle.Text = "";
        }

        private void frmTypeDam_Load(object sender, EventArgs e)
        {
            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            btnSave.Enabled = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text != "" && txtCode.Text != "" && txtTitle.Text != "")
                {

                    using (UnitofWork db = new UnitofWork())
                    {
                        int num = cmbSex.SelectedIndex;
                        tbl_TypeDam typeDam = new tbl_TypeDam()
                        {
                            Code = int.Parse(txtCode.Text),
                            Title = txtTitle.Text,
                            Sex = byte.Parse(num.ToString()),
                            Code_GroupDam = int.Parse(cmbGroup.Text),
                            Description = txtDescription.Text,

                        };
                        db.KoshtargahRepository.InsertTypeDam(typeDam);
                        db.Save();
                        BindGrid();
                    }
                }
                //After Save
                txtCode.Text = "";
                txtTitle.Text = "";
                cmbGroup.Text = "";
                cmbSex.Text = "";
                pnlAdd.Enabled = false;
                btnSave.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("خطا" + ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int num = cmbSex.SelectedIndex;
            using (UnitofWork db = new UnitofWork())
            {
                tbl_TypeDam typeDam = new tbl_TypeDam()
                {
                    Code = int.Parse(txtCode.Text),
                    Title = txtTitle.Text,
                    Sex = byte.Parse(num.ToString()),
                    Code_GroupDam = int.Parse(cmbGroup.Text),
                    Description = txtDescription.Text,

                };
                db.KoshtargahRepository.UpdateTypeDam(typeDam);
                db.Save();
                BindGrid();
            }
            txtCode.Text = "";
            txtTitle.Text = "";
            cmbGroup.Text = "";
            cmbSex.Text = "";
            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgVTypeDam.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgVTypeDam.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف {name} مطمئن هستید ؟", "توجه", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int typeDamId = int.Parse(dgVTypeDam.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteTypeDam(typeDamId);
                        db.Save();
                        BindGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("لطفا یکی از سطرهای بیماری را انتخاب نمایید");
            }
        }

        private void dgVTypeDam_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgVTypeDam.CurrentRow != null)
                {
                    txtCode.Text = dgVTypeDam.CurrentRow.Cells[0].Value.ToString();
                    txtTitle.Text = dgVTypeDam.CurrentRow.Cells[1].Value.ToString();

                    if (dgVTypeDam.CurrentRow.Cells[2].Value != null)
                        cmbGroup.Text = dgVTypeDam.CurrentRow.Cells[2].Value.ToString();
                    if (dgVTypeDam.CurrentRow.Cells[3].Value != null)
                        cmbSex.Text = dgVTypeDam.CurrentRow.Cells[3].Value.ToString();
                    if (dgVTypeDam.CurrentRow.Cells[4].Value != null)
                        txtDescription.Text = dgVTypeDam.CurrentRow.Cells[4].Value.ToString();

                    pnlAdd.Enabled = true;
                    btnEdit.Enabled = true;

                }
            }
            catch
            {
                MessageBox.Show("خطا در لود اطلاعات");
            }
        }

        private void dgVTypeDam_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnSave_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    btnNew_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F8:
                    btnEdit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Delete:
                    btnDel_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;

            }
        }

        private void dgVTypeDam_MouseClick(object sender, MouseEventArgs e)
        {
            btnDel.Enabled = true;
            btnEdit.Enabled = true;
        }
    }
}
