﻿using Koshtargah.DataLayer.Context;
using Koshtargah.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class frmDampezeshk : Form
    {
        

        public frmDampezeshk()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlAdd.Enabled = true;
            btnSave.Enabled = true;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            txtCode.Text = "";
            txtFamily.Text = "";
            txtName.Text = "";
            txtNezamCode.Text = "";
            txtTafsilCode.Text = "";
            txtTel.Text = "";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmDampezeshk_Load(object sender, EventArgs e)
        {
            
            BindGrid();

            pnlAdd.Enabled = false;
            btnSave.Enabled = false;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
        }
        void BindGrid()
        {
            using (UnitofWork db = new UnitofWork())
            {
                dgVDamPezeshk.DataSource = db.KoshtargahRepository.GetAllDampezeshk();
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text!="" && txtFamily.Text!="" && txtName.Text!="")
                {
                    using (UnitofWork db = new UnitofWork())
                    {
                        tbl_Dampezeshk dampezeshk = new tbl_Dampezeshk()
                    {
                        Code = int.Parse(txtCode.Text),
                        Name = txtName.Text,
                        family = txtFamily.Text,
                        nezampezeshki = txtNezamCode.Text,
                        tel = txtTel.Text,
                        codeTafsili = txtTafsilCode.Text
                    };
                    db.KoshtargahRepository.InsertDampezeshk(dampezeshk);
                    db.Save();
                    BindGrid();
                    }
                }
                
                //after save
                txtCode.Text = "";
                txtFamily.Text = "";
                txtName.Text = "";
                txtNezamCode.Text = "";
                txtTafsilCode.Text = "";
                txtTel.Text = "";
                btnSave.Enabled = false;
                pnlAdd.Enabled = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show("خطا"+ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (UnitofWork db = new UnitofWork())
            {
                tbl_Dampezeshk dampezeshk = new tbl_Dampezeshk()
                {
                    Code = int.Parse(txtCode.Text),
                    Name = txtName.Text,
                    family = txtFamily.Text,
                    nezampezeshki = txtNezamCode.Text,
                    tel = txtTel.Text,
                    codeTafsili = txtTafsilCode.Text
                };
                db.KoshtargahRepository.UpdateDampezeshk(dampezeshk);
                db.Save();
                BindGrid();
            }

            //after save

            txtCode.Text = "";
            txtFamily.Text = "";
            txtName.Text = "";
            txtNezamCode.Text = "";
            txtTafsilCode.Text = "";
            txtTel.Text = "";
            btnEdit.Enabled = false;
            pnlAdd.Enabled = false;
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgVDamPezeshk.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgVDamPezeshk.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف {name} مطمئن هستید ؟", "توجه", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int dampezeshkId = int.Parse(dgVDamPezeshk.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteDampezeshk(dampezeshkId);
                        db.Save();
                        BindGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("لطفا یکی از سطرهای بیماری را انتخاب نمایید");
            }
        }

        private void dgVDamPezeshk_MouseClick(object sender, MouseEventArgs e)
        {
            btnDel.Enabled = true;
            btnEdit.Enabled = true;
        }

        private void dgVDamPezeshk_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgVDamPezeshk.CurrentRow != null)
                {
                    txtCode.Text = dgVDamPezeshk.CurrentRow.Cells[0].Value.ToString();
                    txtName.Text = dgVDamPezeshk.CurrentRow.Cells[1].Value.ToString();
                    txtFamily.Text = dgVDamPezeshk.CurrentRow.Cells[2].Value.ToString();
                    if (dgVDamPezeshk.CurrentRow.Cells[3].Value != null)
                        txtNezamCode.Text = dgVDamPezeshk.CurrentRow.Cells[3].Value.ToString();
                    if (dgVDamPezeshk.CurrentRow.Cells[4].Value != null)
                        txtTel.Text = dgVDamPezeshk.CurrentRow.Cells[4].Value.ToString();
                    if (dgVDamPezeshk.CurrentRow.Cells[5].Value != null)
                        txtTafsilCode.Text = dgVDamPezeshk.CurrentRow.Cells[5].Value.ToString();
                    
                    pnlAdd.Enabled = true;
                    btnEdit.Enabled = true;

                }
            }
            catch
            {
                MessageBox.Show("خطا در لود اطلاعات");
            }
        }

        private void dgVDamPezeshk_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnSave_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    btnNew_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F8:
                    btnEdit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Delete:
                    btnDel_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;

            }
        }
    }
}
