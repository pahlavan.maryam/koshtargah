﻿namespace Koshtargah.App
{
    partial class frmGroupDam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroupDam));
            this.cmbNumbering = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtWeightChan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgVGroupDam = new System.Windows.Forms.DataGridView();
            this.pnlAdd = new System.Windows.Forms.Panel();
            this.cmbKhat = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtWeightGar = new System.Windows.Forms.TextBox();
            this.txtWeightAb = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgVGroupDam)).BeginInit();
            this.pnlAdd.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbNumbering
            // 
            this.cmbNumbering.FormattingEnabled = true;
            this.cmbNumbering.Items.AddRange(new object[] {
            "",
            "کلی",
            "به ازای هر دام"});
            this.cmbNumbering.Location = new System.Drawing.Point(210, 35);
            this.cmbNumbering.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbNumbering.Name = "cmbNumbering";
            this.cmbNumbering.Size = new System.Drawing.Size(116, 28);
            this.cmbNumbering.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(335, 39);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "نوع شماره گذاری:";
            // 
            // txtWeightChan
            // 
            this.txtWeightChan.Location = new System.Drawing.Point(20, 7);
            this.txtWeightChan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtWeightChan.Name = "txtWeightChan";
            this.txtWeightChan.Size = new System.Drawing.Size(116, 27);
            this.txtWeightChan.TabIndex = 2;
            this.txtWeightChan.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(579, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "عنوان گروه دام:";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(431, 7);
            this.txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(148, 27);
            this.txtCode.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(607, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "کد گروه:";
            // 
            // dgVGroupDam
            // 
            this.dgVGroupDam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVGroupDam.Location = new System.Drawing.Point(13, 110);
            this.dgVGroupDam.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgVGroupDam.Name = "dgVGroupDam";
            this.dgVGroupDam.Size = new System.Drawing.Size(657, 270);
            this.dgVGroupDam.TabIndex = 8;
            this.dgVGroupDam.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVGroupDam_CellDoubleClick);
            this.dgVGroupDam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgVGroupDam_KeyDown);
            this.dgVGroupDam.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgVGroupDam_MouseClick);
            // 
            // pnlAdd
            // 
            this.pnlAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAdd.Controls.Add(this.cmbKhat);
            this.pnlAdd.Controls.Add(this.cmbNumbering);
            this.pnlAdd.Controls.Add(this.label7);
            this.pnlAdd.Controls.Add(this.label6);
            this.pnlAdd.Controls.Add(this.label5);
            this.pnlAdd.Controls.Add(this.label4);
            this.pnlAdd.Controls.Add(this.label3);
            this.pnlAdd.Controls.Add(this.txtWeightGar);
            this.pnlAdd.Controls.Add(this.txtWeightAb);
            this.pnlAdd.Controls.Add(this.txtTitle);
            this.pnlAdd.Controls.Add(this.txtWeightChan);
            this.pnlAdd.Controls.Add(this.label2);
            this.pnlAdd.Controls.Add(this.txtCode);
            this.pnlAdd.Controls.Add(this.label1);
            this.pnlAdd.Enabled = false;
            this.pnlAdd.Location = new System.Drawing.Point(13, 6);
            this.pnlAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlAdd.Name = "pnlAdd";
            this.pnlAdd.Size = new System.Drawing.Size(657, 102);
            this.pnlAdd.TabIndex = 7;
            // 
            // cmbKhat
            // 
            this.cmbKhat.FormattingEnabled = true;
            this.cmbKhat.Location = new System.Drawing.Point(210, 6);
            this.cmbKhat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbKhat.Name = "cmbKhat";
            this.cmbKhat.Size = new System.Drawing.Size(116, 28);
            this.cmbKhat.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(145, 71);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "وزن گردن:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(145, 39);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "وزن آب:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(145, 10);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "وزن چنگه:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(335, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "خط کشتار:";
            // 
            // txtWeightGar
            // 
            this.txtWeightGar.Location = new System.Drawing.Point(20, 66);
            this.txtWeightGar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtWeightGar.Name = "txtWeightGar";
            this.txtWeightGar.Size = new System.Drawing.Size(116, 27);
            this.txtWeightGar.TabIndex = 2;
            this.txtWeightGar.Text = "0";
            // 
            // txtWeightAb
            // 
            this.txtWeightAb.Location = new System.Drawing.Point(20, 36);
            this.txtWeightAb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtWeightAb.Name = "txtWeightAb";
            this.txtWeightAb.Size = new System.Drawing.Size(116, 27);
            this.txtWeightAb.TabIndex = 2;
            this.txtWeightAb.Text = "0";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(431, 36);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(148, 27);
            this.txtTitle.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnEdit);
            this.panel2.Controls.Add(this.btnExit);
            this.panel2.Controls.Add(this.btnDel);
            this.panel2.Controls.Add(this.btnNew);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Location = new System.Drawing.Point(13, 390);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(657, 55);
            this.panel2.TabIndex = 9;
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnEdit.Image = global::Koshtargah.App.Properties.Resources.edit;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(265, 5);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(125, 42);
            this.btnEdit.TabIndex = 7;
            this.btnEdit.TabStop = false;
            this.btnEdit.Text = "    ویرایش (F8)";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExit.Image = global::Koshtargah.App.Properties.Resources.package_return_512;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(8, 5);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(125, 42);
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "    بازگشت (F4)";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDel.Image = ((System.Drawing.Image)(resources.GetObject("btnDel.Image")));
            this.btnDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.Location = new System.Drawing.Point(135, 5);
            this.btnDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(125, 42);
            this.btnDel.TabIndex = 2;
            this.btnDel.TabStop = false;
            this.btnDel.Text = "    حذف (Ctrl+Del)";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnNew
            // 
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNew.Location = new System.Drawing.Point(389, 5);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(125, 42);
            this.btnNew.TabIndex = 5;
            this.btnNew.TabStop = false;
            this.btnNew.Text = "جدید (F5)";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSave.Image = global::Koshtargah.App.Properties.Resources.save_download_icon_10;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(516, 5);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(125, 42);
            this.btnSave.TabIndex = 6;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "ذخیره (F2)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmGroupDam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(680, 454);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dgVGroupDam);
            this.Controls.Add(this.pnlAdd);
            this.Font = new System.Drawing.Font("B Homa", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGroupDam";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "     گروه دام";
            this.Load += new System.EventHandler(this.frmGroupDam_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgVGroupDam)).EndInit();
            this.pnlAdd.ResumeLayout(false);
            this.pnlAdd.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbNumbering;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtWeightChan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgVGroupDam;
        private System.Windows.Forms.Panel pnlAdd;
        private System.Windows.Forms.ComboBox cmbKhat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWeightGar;
        private System.Windows.Forms.TextBox txtWeightAb;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEdit;
    }
}