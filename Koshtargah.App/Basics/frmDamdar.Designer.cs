﻿namespace Koshtargah.App
{
    partial class frmDamdar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDamdar));
            this.pnlDamdar = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_Damdar = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDateB = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSerial = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtIdentifier = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBD = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.Panel();
            this.rdDisable = new System.Windows.Forms.RadioButton();
            this.rdEnable = new System.Windows.Forms.RadioButton();
            this.txtTafsili2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTafsili1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBPlace = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDSodor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFather = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFamily = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tp_Contacs = new System.Windows.Forms.TabPage();
            this.txtPostalCode = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tp_Bank = new System.Windows.Forms.TabPage();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBankName = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtBankCard = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBankID = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tp_Attach = new System.Windows.Forms.TabPage();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.dgDamdar = new System.Windows.Forms.DataGridView();
            this.codeDamdarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.familyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblDamdarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.koshtargah_DBDataSet = new Koshtargah.App.Koshtargah_DBDataSet();
            this.tbl_DamdarTableAdapter = new Koshtargah.App.Koshtargah_DBDataSetTableAdapters.tbl_DamdarTableAdapter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.requiredFieldValidator1 = new ValidationComponents.RequiredFieldValidator(this.components);
            this.requiredFieldValidator2 = new ValidationComponents.RequiredFieldValidator(this.components);
            this.pnlDamdar.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tp_Damdar.SuspendLayout();
            this.status.SuspendLayout();
            this.tp_Contacs.SuspendLayout();
            this.tp_Bank.SuspendLayout();
            this.tp_Attach.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDamdar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDamdarBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.koshtargah_DBDataSet)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDamdar
            // 
            this.pnlDamdar.Controls.Add(this.tabControl1);
            this.pnlDamdar.Location = new System.Drawing.Point(4, 6);
            this.pnlDamdar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlDamdar.Name = "pnlDamdar";
            this.pnlDamdar.Size = new System.Drawing.Size(541, 469);
            this.pnlDamdar.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tp_Damdar);
            this.tabControl1.Controls.Add(this.tp_Contacs);
            this.tabControl1.Controls.Add(this.tp_Bank);
            this.tabControl1.Controls.Add(this.tp_Attach);
            this.tabControl1.Location = new System.Drawing.Point(4, 5);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(533, 461);
            this.tabControl1.TabIndex = 0;
            // 
            // tp_Damdar
            // 
            this.tp_Damdar.Controls.Add(this.label14);
            this.tp_Damdar.Controls.Add(this.txtDateB);
            this.tp_Damdar.Controls.Add(this.label13);
            this.tp_Damdar.Controls.Add(this.txtSerial);
            this.tp_Damdar.Controls.Add(this.label12);
            this.tp_Damdar.Controls.Add(this.txtIdentifier);
            this.tp_Damdar.Controls.Add(this.label11);
            this.tp_Damdar.Controls.Add(this.txtBD);
            this.tp_Damdar.Controls.Add(this.label10);
            this.tp_Damdar.Controls.Add(this.status);
            this.tp_Damdar.Controls.Add(this.txtTafsili2);
            this.tp_Damdar.Controls.Add(this.label9);
            this.tp_Damdar.Controls.Add(this.txtTafsili1);
            this.tp_Damdar.Controls.Add(this.label7);
            this.tp_Damdar.Controls.Add(this.txtBPlace);
            this.tp_Damdar.Controls.Add(this.label6);
            this.tp_Damdar.Controls.Add(this.txtDSodor);
            this.tp_Damdar.Controls.Add(this.label5);
            this.tp_Damdar.Controls.Add(this.txtNC);
            this.tp_Damdar.Controls.Add(this.label4);
            this.tp_Damdar.Controls.Add(this.txtFather);
            this.tp_Damdar.Controls.Add(this.label3);
            this.tp_Damdar.Controls.Add(this.txtFamily);
            this.tp_Damdar.Controls.Add(this.label2);
            this.tp_Damdar.Controls.Add(this.txtName);
            this.tp_Damdar.Controls.Add(this.label1);
            this.tp_Damdar.Location = new System.Drawing.Point(4, 34);
            this.tp_Damdar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_Damdar.Name = "tp_Damdar";
            this.tp_Damdar.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_Damdar.Size = new System.Drawing.Size(525, 423);
            this.tp_Damdar.TabIndex = 0;
            this.tp_Damdar.Text = "اطلاعات شناسنامه‌ای";
            this.tp_Damdar.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(164, 119);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 25);
            this.label14.TabIndex = 28;
            this.label14.Text = "کد تفصیلی2: ";
            // 
            // txtDateB
            // 
            this.txtDateB.Location = new System.Drawing.Point(265, 383);
            this.txtDateB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDateB.Name = "txtDateB";
            this.txtDateB.Size = new System.Drawing.Size(132, 32);
            this.txtDateB.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(401, 350);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 25);
            this.label13.TabIndex = 26;
            this.label13.Text = "محل صدور شناسنامه:";
            // 
            // txtSerial
            // 
            this.txtSerial.Location = new System.Drawing.Point(265, 225);
            this.txtSerial.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.Size = new System.Drawing.Size(132, 32);
            this.txtSerial.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(421, 233);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 25);
            this.label12.TabIndex = 24;
            this.label12.Text = "سریال شناسنامه:";
            // 
            // txtIdentifier
            // 
            this.txtIdentifier.Location = new System.Drawing.Point(265, 183);
            this.txtIdentifier.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIdentifier.Name = "txtIdentifier";
            this.txtIdentifier.Size = new System.Drawing.Size(132, 32);
            this.txtIdentifier.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(400, 310);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 25);
            this.label11.TabIndex = 22;
            this.label11.Text = "تاریخ صدور شناسنامه:";
            // 
            // txtBD
            // 
            this.txtBD.Location = new System.Drawing.Point(265, 145);
            this.txtBD.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBD.Name = "txtBD";
            this.txtBD.Size = new System.Drawing.Size(132, 32);
            this.txtBD.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(456, 149);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "محل تولد:";
            // 
            // status
            // 
            this.status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.status.Controls.Add(this.rdDisable);
            this.status.Controls.Add(this.rdEnable);
            this.status.Location = new System.Drawing.Point(23, 178);
            this.status.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(142, 45);
            this.status.TabIndex = 19;
            // 
            // rdDisable
            // 
            this.rdDisable.AutoSize = true;
            this.rdDisable.Location = new System.Drawing.Point(3, 7);
            this.rdDisable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdDisable.Name = "rdDisable";
            this.rdDisable.Size = new System.Drawing.Size(75, 29);
            this.rdDisable.TabIndex = 18;
            this.rdDisable.TabStop = true;
            this.rdDisable.Text = "غیرفعال";
            this.rdDisable.UseVisualStyleBackColor = true;
            // 
            // rdEnable
            // 
            this.rdEnable.AutoSize = true;
            this.rdEnable.Location = new System.Drawing.Point(80, 8);
            this.rdEnable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdEnable.Name = "rdEnable";
            this.rdEnable.Size = new System.Drawing.Size(59, 29);
            this.rdEnable.TabIndex = 18;
            this.rdEnable.TabStop = true;
            this.rdEnable.Text = "فعال";
            this.rdEnable.UseVisualStyleBackColor = true;
            // 
            // txtTafsili2
            // 
            this.txtTafsili2.Location = new System.Drawing.Point(23, 116);
            this.txtTafsili2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTafsili2.Name = "txtTafsili2";
            this.txtTafsili2.Size = new System.Drawing.Size(132, 32);
            this.txtTafsili2.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(191, 191);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 25);
            this.label9.TabIndex = 2;
            this.label9.Text = "وضعیت";
            // 
            // txtTafsili1
            // 
            this.txtTafsili1.Location = new System.Drawing.Point(23, 42);
            this.txtTafsili1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTafsili1.Name = "txtTafsili1";
            this.txtTafsili1.Size = new System.Drawing.Size(132, 32);
            this.txtTafsili1.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(173, 49);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 25);
            this.label7.TabIndex = 3;
            this.label7.Text = "کد تفصیلی:";
            // 
            // txtBPlace
            // 
            this.txtBPlace.Location = new System.Drawing.Point(265, 346);
            this.txtBPlace.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBPlace.Name = "txtBPlace";
            this.txtBPlace.Size = new System.Drawing.Size(132, 32);
            this.txtBPlace.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(452, 388);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 25);
            this.label6.TabIndex = 4;
            this.label6.Text = "تاریخ تولد:";
            // 
            // txtDSodor
            // 
            this.txtDSodor.Location = new System.Drawing.Point(265, 306);
            this.txtDSodor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDSodor.Name = "txtDSodor";
            this.txtDSodor.Size = new System.Drawing.Size(132, 32);
            this.txtDSodor.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(463, 269);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 25);
            this.label5.TabIndex = 5;
            this.label5.Text = "کد ملی:";
            // 
            // txtNC
            // 
            this.txtNC.Location = new System.Drawing.Point(265, 266);
            this.txtNC.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNC.Name = "txtNC";
            this.txtNC.Size = new System.Drawing.Size(132, 32);
            this.txtNC.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(417, 188);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "شماره شناسنامه:";
            // 
            // txtFather
            // 
            this.txtFather.Location = new System.Drawing.Point(265, 103);
            this.txtFather.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFather.Name = "txtFather";
            this.txtFather.Size = new System.Drawing.Size(132, 32);
            this.txtFather.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(466, 108);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "نام پدر:";
            // 
            // txtFamily
            // 
            this.txtFamily.Location = new System.Drawing.Point(265, 63);
            this.txtFamily.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFamily.Name = "txtFamily";
            this.txtFamily.Size = new System.Drawing.Size(132, 32);
            this.txtFamily.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(442, 68);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "نام خانوادگی:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(265, 23);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(132, 32);
            this.txtName.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(487, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "نام:";
            // 
            // tp_Contacs
            // 
            this.tp_Contacs.Controls.Add(this.txtPostalCode);
            this.tp_Contacs.Controls.Add(this.label18);
            this.tp_Contacs.Controls.Add(this.txtAddress);
            this.tp_Contacs.Controls.Add(this.label15);
            this.tp_Contacs.Controls.Add(this.txtMobile);
            this.tp_Contacs.Controls.Add(this.label16);
            this.tp_Contacs.Controls.Add(this.txtTel);
            this.tp_Contacs.Controls.Add(this.label17);
            this.tp_Contacs.Location = new System.Drawing.Point(4, 34);
            this.tp_Contacs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_Contacs.Name = "tp_Contacs";
            this.tp_Contacs.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_Contacs.Size = new System.Drawing.Size(525, 423);
            this.tp_Contacs.TabIndex = 1;
            this.tp_Contacs.Text = "اطلاعات ارتباطی";
            this.tp_Contacs.UseVisualStyleBackColor = true;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Location = new System.Drawing.Point(268, 123);
            this.txtPostalCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(132, 32);
            this.txtPostalCode.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(430, 127);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 25);
            this.label18.TabIndex = 24;
            this.label18.Text = "کدپستی";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(24, 175);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(376, 55);
            this.txtAddress.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(439, 175);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 25);
            this.label15.TabIndex = 18;
            this.label15.Text = "آدرس";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(268, 73);
            this.txtMobile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(132, 32);
            this.txtMobile.TabIndex = 22;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(408, 76);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 25);
            this.label16.TabIndex = 19;
            this.label16.Text = "شماره موبایل";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(268, 25);
            this.txtTel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(132, 32);
            this.txtTel.TabIndex = 23;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(411, 26);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 25);
            this.label17.TabIndex = 20;
            this.label17.Text = "شماره تماس";
            // 
            // tp_Bank
            // 
            this.tp_Bank.Controls.Add(this.textBox8);
            this.tp_Bank.Controls.Add(this.label22);
            this.tp_Bank.Controls.Add(this.txtBankName);
            this.tp_Bank.Controls.Add(this.label19);
            this.tp_Bank.Controls.Add(this.txtBankCard);
            this.tp_Bank.Controls.Add(this.label20);
            this.tp_Bank.Controls.Add(this.txtBankID);
            this.tp_Bank.Controls.Add(this.label21);
            this.tp_Bank.Location = new System.Drawing.Point(4, 34);
            this.tp_Bank.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_Bank.Name = "tp_Bank";
            this.tp_Bank.Size = new System.Drawing.Size(525, 423);
            this.tp_Bank.TabIndex = 2;
            this.tp_Bank.Text = "اطلاعات بانکی";
            this.tp_Bank.UseVisualStyleBackColor = true;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(265, 185);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(132, 32);
            this.textBox8.TabIndex = 33;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(395, 188);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(93, 25);
            this.label22.TabIndex = 32;
            this.label22.Text = "نام دارنده حساب";
            // 
            // txtBankName
            // 
            this.txtBankName.Location = new System.Drawing.Point(265, 132);
            this.txtBankName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Size = new System.Drawing.Size(132, 32);
            this.txtBankName.TabIndex = 31;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(427, 136);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 25);
            this.label19.TabIndex = 30;
            this.label19.Text = "نام بانک";
            // 
            // txtBankCard
            // 
            this.txtBankCard.Location = new System.Drawing.Point(265, 82);
            this.txtBankCard.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBankCard.Name = "txtBankCard";
            this.txtBankCard.Size = new System.Drawing.Size(132, 32);
            this.txtBankCard.TabIndex = 28;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(405, 85);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 25);
            this.label20.TabIndex = 26;
            this.label20.Text = "شماره کارت";
            // 
            // txtBankID
            // 
            this.txtBankID.Location = new System.Drawing.Point(265, 34);
            this.txtBankID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBankID.Name = "txtBankID";
            this.txtBankID.Size = new System.Drawing.Size(132, 32);
            this.txtBankID.TabIndex = 29;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(408, 35);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 25);
            this.label21.TabIndex = 27;
            this.label21.Text = "شماره حساب";
            // 
            // tp_Attach
            // 
            this.tp_Attach.Controls.Add(this.textBox11);
            this.tp_Attach.Controls.Add(this.label25);
            this.tp_Attach.Controls.Add(this.textBox10);
            this.tp_Attach.Controls.Add(this.label24);
            this.tp_Attach.Controls.Add(this.textBox9);
            this.tp_Attach.Controls.Add(this.label23);
            this.tp_Attach.Location = new System.Drawing.Point(4, 34);
            this.tp_Attach.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_Attach.Name = "tp_Attach";
            this.tp_Attach.Size = new System.Drawing.Size(525, 423);
            this.tp_Attach.TabIndex = 3;
            this.tp_Attach.Text = "پیوست‌ها";
            this.tp_Attach.UseVisualStyleBackColor = true;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(271, 132);
            this.textBox11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(132, 32);
            this.textBox11.TabIndex = 37;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(416, 135);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(51, 25);
            this.label25.TabIndex = 36;
            this.label25.Text = "بارگذاری";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(271, 95);
            this.textBox10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(132, 32);
            this.textBox10.TabIndex = 35;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(433, 99);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 25);
            this.label24.TabIndex = 34;
            this.label24.Text = "نام ";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(271, 58);
            this.textBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(132, 32);
            this.textBox9.TabIndex = 33;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(411, 58);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 25);
            this.label23.TabIndex = 32;
            this.label23.Text = "نوع مدرک";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtSearch);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.dgDamdar);
            this.panel2.Location = new System.Drawing.Point(553, 6);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(215, 469);
            this.panel2.TabIndex = 0;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(4, 5);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(163, 32);
            this.txtSearch.TabIndex = 29;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(175, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(46, 25);
            this.label26.TabIndex = 10;
            this.label26.Text = "جستجو";
            // 
            // dgDamdar
            // 
            this.dgDamdar.AllowUserToAddRows = false;
            this.dgDamdar.AllowUserToDeleteRows = false;
            this.dgDamdar.AutoGenerateColumns = false;
            this.dgDamdar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDamdar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codeDamdarDataGridViewTextBoxColumn,
            this.familyDataGridViewTextBoxColumn,
            this.Column1});
            this.dgDamdar.DataSource = this.tblDamdarBindingSource;
            this.dgDamdar.Location = new System.Drawing.Point(-5, 40);
            this.dgDamdar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgDamdar.Name = "dgDamdar";
            this.dgDamdar.ReadOnly = true;
            this.dgDamdar.Size = new System.Drawing.Size(214, 427);
            this.dgDamdar.TabIndex = 0;
            // 
            // codeDamdarDataGridViewTextBoxColumn
            // 
            this.codeDamdarDataGridViewTextBoxColumn.DataPropertyName = "Code_Damdar";
            this.codeDamdarDataGridViewTextBoxColumn.FillWeight = 60F;
            this.codeDamdarDataGridViewTextBoxColumn.HeaderText = "کد دامدار";
            this.codeDamdarDataGridViewTextBoxColumn.Name = "codeDamdarDataGridViewTextBoxColumn";
            this.codeDamdarDataGridViewTextBoxColumn.ReadOnly = true;
            this.codeDamdarDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codeDamdarDataGridViewTextBoxColumn.Width = 70;
            // 
            // familyDataGridViewTextBoxColumn
            // 
            this.familyDataGridViewTextBoxColumn.DataPropertyName = "Family";
            this.familyDataGridViewTextBoxColumn.FillWeight = 60F;
            this.familyDataGridViewTextBoxColumn.HeaderText = "نام خانوادگی";
            this.familyDataGridViewTextBoxColumn.Name = "familyDataGridViewTextBoxColumn";
            this.familyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Name";
            this.Column1.HeaderText = "نام";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // tblDamdarBindingSource
            // 
            this.tblDamdarBindingSource.DataMember = "tbl_Damdar";
            this.tblDamdarBindingSource.DataSource = this.koshtargah_DBDataSet;
            // 
            // koshtargah_DBDataSet
            // 
            this.koshtargah_DBDataSet.DataSetName = "Koshtargah_DBDataSet";
            this.koshtargah_DBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbl_DamdarTableAdapter
            // 
            this.tbl_DamdarTableAdapter.ClearBeforeFill = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnEdit);
            this.panel3.Controls.Add(this.btnExit);
            this.panel3.Controls.Add(this.btnDel);
            this.panel3.Controls.Add(this.btnNew);
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Location = new System.Drawing.Point(4, 482);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(743, 56);
            this.panel3.TabIndex = 13;
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnEdit.Image = global::Koshtargah.App.Properties.Resources.edit;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(300, 5);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(140, 42);
            this.btnEdit.TabIndex = 7;
            this.btnEdit.TabStop = false;
            this.btnEdit.Text = "    ویرایش (F8)";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExit.Image = global::Koshtargah.App.Properties.Resources.package_return_512;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(4, 5);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(140, 42);
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "    بازگشت (F4)";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDel.Image = ((System.Drawing.Image)(resources.GetObject("btnDel.Image")));
            this.btnDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.Location = new System.Drawing.Point(152, 5);
            this.btnDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(140, 42);
            this.btnDel.TabIndex = 2;
            this.btnDel.TabStop = false;
            this.btnDel.Text = "    حذف (Ctrl+Del)";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnNew
            // 
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNew.Location = new System.Drawing.Point(448, 5);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(140, 42);
            this.btnNew.TabIndex = 5;
            this.btnNew.TabStop = false;
            this.btnNew.Text = "جدید (F5)";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSave.Image = global::Koshtargah.App.Properties.Resources.save_download_icon_10;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(596, 5);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(140, 42);
            this.btnSave.TabIndex = 6;
            this.btnSave.TabStop = false;
            this.btnSave.Text = "ذخیره (F2)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // requiredFieldValidator1
            // 
            this.requiredFieldValidator1.CancelFocusChangeWhenInvalid = false;
            this.requiredFieldValidator1.ControlToValidate = this.txtName;
            this.requiredFieldValidator1.ErrorMessage = "لطفا نام را وارد کنید";
            this.requiredFieldValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("requiredFieldValidator1.Icon")));
            // 
            // requiredFieldValidator2
            // 
            this.requiredFieldValidator2.CancelFocusChangeWhenInvalid = false;
            this.requiredFieldValidator2.ControlToValidate = this.txtFamily;
            this.requiredFieldValidator2.ErrorMessage = "لطفا فامیل را وارد کنید";
            this.requiredFieldValidator2.Icon = ((System.Drawing.Icon)(resources.GetObject("requiredFieldValidator2.Icon")));
            // 
            // frmDamdar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(782, 553);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlDamdar);
            this.Font = new System.Drawing.Font("B Homa", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDamdar";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تعریف دامدار";
            this.Load += new System.EventHandler(this.M_Damdar_Load);
            this.pnlDamdar.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tp_Damdar.ResumeLayout(false);
            this.tp_Damdar.PerformLayout();
            this.status.ResumeLayout(false);
            this.status.PerformLayout();
            this.tp_Contacs.ResumeLayout(false);
            this.tp_Contacs.PerformLayout();
            this.tp_Bank.ResumeLayout(false);
            this.tp_Bank.PerformLayout();
            this.tp_Attach.ResumeLayout(false);
            this.tp_Attach.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDamdar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblDamdarBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.koshtargah_DBDataSet)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDamdar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgDamdar;
        private Koshtargah_DBDataSet koshtargah_DBDataSet;
        private System.Windows.Forms.BindingSource tblDamdarBindingSource;
        private Koshtargah_DBDataSetTableAdapters.tbl_DamdarTableAdapter tbl_DamdarTableAdapter;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp_Damdar;
        private System.Windows.Forms.TextBox txtDateB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSerial;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtIdentifier;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel status;
        private System.Windows.Forms.RadioButton rdDisable;
        private System.Windows.Forms.RadioButton rdEnable;
        private System.Windows.Forms.TextBox txtTafsili2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTafsili1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBPlace;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDSodor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFather;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFamily;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tp_Contacs;
        private System.Windows.Forms.TabPage tp_Bank;
        private System.Windows.Forms.TabPage tp_Attach;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPostalCode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtBankName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtBankCard;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBankID;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDamdarDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn familyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private ValidationComponents.RequiredFieldValidator requiredFieldValidator1;
        private ValidationComponents.RequiredFieldValidator requiredFieldValidator2;
    }
}