﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Koshtargah.DataLayer;
using Koshtargah.DataLayer.Context;
using Koshtargah.DataLayer.Repositories;
using Koshtargah.DataLayer.Services;
using ValidationComponents;

namespace Koshtargah.App
{
    public partial class frmKharidaran : Form
    {
        private int kharidar_id = 0;
        UnitofWork db = new UnitofWork();
        public frmKharidaran()
        {
            InitializeComponent();
        }
        void BindGrid()
        {
            using (db)
            {
                dgvKharidaran.AutoGenerateColumns = false;
                dgvKharidaran.DataSource = db.KoshtargahRepository.GetAllKharidar();
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void frmKharidaran_Load(object sender, EventArgs e)
        {
            UnitofWork db = new UnitofWork();
            BindGrid();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (BaseValidator.IsFormValid(this.components))
            {
                tbl_Kharidaran kharidar = new tbl_Kharidaran()
                {

                };
                db.KoshtargahRepository.InsertKharidar(kharidar);
                db.Save();
                BindGrid();
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgvKharidaran.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgvKharidaran.CurrentRow.Cells[2].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف رکورد {name}مطمپن هستید؟ ", "توجه", MessageBoxButtons.YesNo
                        , MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int kharidarCode = int.Parse(dgvKharidaran.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteKharidar(kharidarCode);
                        db.Save();
                        BindGrid();
                    }
                }
            }
            else
            {
                RtlMessageBox.Show("ابتدا رکورد مورد نظر را انتخاب کنید");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvKharidaran.CurrentRow != null)
            {

                this.Text = "ویرایش مشخصات";
                btnSave.Text = "  ثبت ویرایش";
                string name = dgvKharidaran.CurrentRow.Cells[2].Value.ToString();

                int kharidarCode = int.Parse(dgvKharidaran.CurrentRow.Cells[0].Value.ToString());
                var kharidar = db.KoshtargahRepository.GetKharidarById(kharidarCode);
                txtCode.Text = kharidar.Code.ToString();
                txtName.Text = kharidar.Name;
                //.....

                db.KoshtargahRepository.DeleteKharidar(kharidarCode);
                db.Save();
                BindGrid();
            }
            else
            {
                RtlMessageBox.Show("ابتدا رکورد مورد نظر را انتخاب کنید");
            }
        }
    }
}
