﻿using Koshtargah.DataLayer;
using Koshtargah.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class frmLineKoshtar : Form
    {
        public frmLineKoshtar()
        {
            InitializeComponent();
        }

        void BindGrid()
        {
            using (UnitofWork db = new UnitofWork())
            {
                dgVLineKoshtar.DataSource = db.KoshtargahRepository.GetAllLineKoshtar();
            }
        }
        private void frmLineKoshtar_Load(object sender, EventArgs e)
        {
            BindGrid();
            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            btnSave.Enabled = false;

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlAdd.Enabled = true;
            btnSave.Enabled = true;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            txtCode.Text = "";
            txtTitle.Text = "";

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text != "" && txtTitle.Text != "")
                {

                    using (UnitofWork db = new UnitofWork())
                    {
                        
                        tbl_LineKoshtar line = new tbl_LineKoshtar()
                        {
                            Code = int.Parse(txtCode.Text),
                            Name = txtTitle.Text,
                        };
                        db.KoshtargahRepository.InsertLineKoshtar(line);
                        db.Save();
                        BindGrid();
                    }

                }
                //After Save
                txtCode.Text = "";
                txtTitle.Text = "";
                btnSave.Enabled = false;
                pnlAdd.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("خطا" + ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (UnitofWork db = new UnitofWork())
            {
                tbl_LineKoshtar line = new tbl_LineKoshtar()
                {
                    Code = int.Parse(txtCode.Text),
                    Name = txtTitle.Text,
                };
                db.KoshtargahRepository.UpdateLineKoshtar(line);
                db.Save();
                BindGrid();
            }
            //After Save
            txtCode.Text = "";
            txtTitle.Text = "";
            btnEdit.Enabled = false;
            pnlAdd.Enabled = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgVLineKoshtar.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgVLineKoshtar.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف {name} مطمئن هستید ؟", "توجه", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int lineId = int.Parse(dgVLineKoshtar.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteLineKoshtar(lineId);
                        db.Save();
                        BindGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("لطفا یکی از سطرهای بیماری را انتخاب نمایید");
            }
        }

        private void dgVLineKoshtar_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgVLineKoshtar.CurrentRow != null)
                {
                    txtCode.Text = dgVLineKoshtar.CurrentRow.Cells[0].Value.ToString();
                    txtTitle.Text = dgVLineKoshtar.CurrentRow.Cells[1].Value.ToString();

                    pnlAdd.Enabled = true;
                    btnEdit.Enabled = true;
                }
            }
            catch
            {
                MessageBox.Show("خطا در لود اطلاعات");
            }
        }

        private void dgVLineKoshtar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnSave_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    btnNew_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F8:
                    btnEdit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Delete:
                    btnDel_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;

            }
        }

        private void dgVLineKoshtar_MouseClick(object sender, MouseEventArgs e)
        {
            btnDel.Enabled = true;
            btnEdit.Enabled = true;
        }
    }
}
