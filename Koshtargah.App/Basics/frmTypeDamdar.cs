﻿using Koshtargah.DataLayer;
using Koshtargah.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class frmTypeDamdar : Form
    {
        public frmTypeDamdar()
        {
            InitializeComponent();
        }

        void BindGrid()
        {
            using (UnitofWork db = new UnitofWork())
            {
                dgVTypeDamdar.DataSource = db.KoshtargahRepository.GetAllTypeDamdar();
            }
        }

        private void frmTypeDamdar_Load(object sender, EventArgs e)
        {
            pnlAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            btnSave.Enabled = false;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlAdd.Enabled = true;
            btnSave.Enabled = true;
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
            txtCode.Text = "";
            //txtTitle.Text = "";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text != "" && txtCode.Text != "" && txtType.Text != "")
                {
                    using (UnitofWork db = new UnitofWork())
                    {
                        tbl_TypeDamdar typeDamdar = new tbl_TypeDamdar()
                        {
                            Code_TypeDamdar = int.Parse(txtCode.Text),
                            TypeDamdar = txtType.Text,
                            Description = txtDescription.Text,

                        };
                        db.KoshtargahRepository.InsertTypeDamdar(typeDamdar);
                        db.Save();
                        BindGrid();
                    }
                }
                //After Save
                btnSave.Enabled = false;
                txtCode.Text = "";
                txtType.Text = "";
                pnlAdd.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("خطا" + ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (UnitofWork db = new UnitofWork())
            {
                tbl_TypeDamdar typedamdar = new tbl_TypeDamdar()
                {
                    Code_TypeDamdar = int.Parse(txtCode.Text),
                    TypeDamdar = txtType.Text,
                    Description = txtDescription.Text,

                };
                db.KoshtargahRepository.UpdateTypeDamdar(typedamdar);
                db.Save();
                BindGrid();
            }

            //after save
            btnEdit.Enabled = false;
            txtCode.Text = "";
            txtType.Text = "";
            pnlAdd.Enabled = false;
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dgVTypeDamdar.CurrentRow != null)
            {
                using (UnitofWork db = new UnitofWork())
                {
                    string name = dgVTypeDamdar.CurrentRow.Cells[1].Value.ToString();
                    if (RtlMessageBox.Show($"آیا از حذف {name} مطمئن هستید ؟", "توجه", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        int typeDamadarId = int.Parse(dgVTypeDamdar.CurrentRow.Cells[0].Value.ToString());
                        db.KoshtargahRepository.DeleteTypeDamdar(typeDamadarId);
                        db.Save();
                        BindGrid();
                    }

                }
            }
            else
            {
                MessageBox.Show("لطفا یکی از سطرهای بیماری را انتخاب نمایید");
            }
        }

        private void dgVTypeDamdar_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgVTypeDamdar.CurrentRow != null)
                {
                    txtCode.Text = dgVTypeDamdar.CurrentRow.Cells[0].Value.ToString();
                    txtType.Text = dgVTypeDamdar.CurrentRow.Cells[1].Value.ToString();

                    if (dgVTypeDamdar.CurrentRow.Cells[2].Value != null)
                        txtDescription.Text = dgVTypeDamdar.CurrentRow.Cells[2].Value.ToString();


                    pnlAdd.Enabled = true;
                    btnEdit.Enabled = true;

                }
            }
            catch
            {
                MessageBox.Show("خطا در لود اطلاعات");
            }
        }

        private void dgVTypeDamdar_MouseClick(object sender, MouseEventArgs e)
        {
            btnDel.Enabled = true;
            btnEdit.Enabled = true;
        }

        private void dgVTypeDamdar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F2:
                    btnSave_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F4:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F5:
                    btnNew_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.F8:
                    btnEdit_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Delete:
                    btnDel_Click(sender, e);
                    e.Handled = true;
                    break;
                case Keys.Escape:
                    btnExit_Click(sender, e);
                    e.Handled = true;
                    break;

            }
        }
    }
}
