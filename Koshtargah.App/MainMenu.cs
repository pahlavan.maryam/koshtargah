﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koshtargah.App
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void tsm_TypeDamdar_Click(object sender, EventArgs e)
        {
            frmTypeDamdar frm = new frmTypeDamdar();
            frm.ShowDialog();
        }

        private void tsm_Damdar_Click(object sender, EventArgs e)
        {
            //frmDamdar frm = new frmDamdar();
            //frm.ShowDialog();
        }

        private void tsm_LineKoshtar_Click(object sender, EventArgs e)
        {
            frmLineKoshtar frm = new frmLineKoshtar();
            frm.ShowDialog();
        }

        private void tsm_Kharidaran_Click(object sender, EventArgs e)
        {
            //frmKharidaran frm = new frmKharidaran();
            //frm.ShowDialog();
        }

        private void tsm_Dampezeshk_Click(object sender, EventArgs e)
        {
            frmDampezeshk frm = new frmDampezeshk();
            frm.ShowDialog();
        }

        private void tsm_GroupDam_Click(object sender, EventArgs e)
        {
            frmGroupDam frm = new frmGroupDam();
            frm.ShowDialog();
        }

        private void tsm_TypeDam_Click(object sender, EventArgs e)
        {
            frmTypeDam frm = new frmTypeDam();
            frm.ShowDialog();
        }

        private void tsm_Bimari_Click(object sender, EventArgs e)
        {
            frmBimari frm = new frmBimari();
            frm.ShowDialog();
        }

        private void tsm_TypeAlayesh_Click(object sender, EventArgs e)
        {
            
        }

        private void tsm_MosavabatAlayesh_Click(object sender, EventArgs e)
        {

        }

        private void tsm_Places_Click(object sender, EventArgs e)
        {
            frmPlaces frm = new frmPlaces();
            frm.ShowDialog();
        }

        private void tsm_ANAlayesh_Click(object sender, EventArgs e)
        {

        }
    }
}
