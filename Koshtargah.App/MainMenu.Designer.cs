﻿namespace Koshtargah.App
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsm_TypeDamdar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Damdar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_LineKoshtar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_Kharidaran = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Dampezeshk = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_GroupDam = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_TypeDam = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Bimari = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_TypeAlayesh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_MosavabatAlayesh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Places = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_ANAlayesh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsm_Entry = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_EntryLine = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Tozin = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_MohasebeAlayesh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_BedehiDamdar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_BankPayment = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_SoratJalaseKharid = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsm_Report_Daily = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_DetailsDaily = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_GroupDaily = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_Report_EntryDaily = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_KolKoshtar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_HesabDamdar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_KharidAlayesh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_Kharidar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_SoratJalaseKharid = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_DetailsSoratJalase = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Report_Places = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsm_Setting_EntryFee = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_Setting_Users = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton5 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsm_Utilities_Period = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_Utilities_Backup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Utilities_Restore = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_Utilities_Move = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.tsm_Utilities_Calc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Utilities_About = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btn_Entry = new System.Windows.Forms.ToolStripButton();
            this.btn_EntryLine = new System.Windows.Forms.ToolStripButton();
            this.btn_Tozin = new System.Windows.Forms.ToolStripButton();
            this.btn_MohasebeAlayesh = new System.Windows.Forms.ToolStripButton();
            this.btn_CardDamdar = new System.Windows.Forms.ToolStripButton();
            this.btn_Bedehi = new System.Windows.Forms.ToolStripButton();
            this.btn_Exit = new System.Windows.Forms.ToolStripButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.toolStrip1.Font = new System.Drawing.Font("B Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.toolStripDropDownButton3,
            this.toolStripDropDownButton4,
            this.toolStripDropDownButton5,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(784, 29);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_TypeDamdar,
            this.tsm_Damdar,
            this.tsm_LineKoshtar,
            this.toolStripMenuItem3,
            this.tsm_Kharidaran,
            this.tsm_Dampezeshk,
            this.tsm_GroupDam,
            this.tsm_TypeDam,
            this.tsm_Bimari,
            this.tsm_TypeAlayesh,
            this.tsm_MosavabatAlayesh,
            this.tsm_Places,
            this.toolStripMenuItem4,
            this.tsm_ANAlayesh});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(46, 26);
            this.toolStripDropDownButton1.Text = "مبانی";
            // 
            // tsm_TypeDamdar
            // 
            this.tsm_TypeDamdar.Name = "tsm_TypeDamdar";
            this.tsm_TypeDamdar.Size = new System.Drawing.Size(192, 26);
            this.tsm_TypeDamdar.Text = "لیست انواع دامداران";
            this.tsm_TypeDamdar.Click += new System.EventHandler(this.tsm_TypeDamdar_Click);
            // 
            // tsm_Damdar
            // 
            this.tsm_Damdar.Name = "tsm_Damdar";
            this.tsm_Damdar.Size = new System.Drawing.Size(192, 26);
            this.tsm_Damdar.Text = "لیست دامداران";
            this.tsm_Damdar.Click += new System.EventHandler(this.tsm_Damdar_Click);
            // 
            // tsm_LineKoshtar
            // 
            this.tsm_LineKoshtar.Name = "tsm_LineKoshtar";
            this.tsm_LineKoshtar.Size = new System.Drawing.Size(192, 26);
            this.tsm_LineKoshtar.Text = "خطوط کشتار";
            this.tsm_LineKoshtar.Click += new System.EventHandler(this.tsm_LineKoshtar_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(189, 6);
            // 
            // tsm_Kharidaran
            // 
            this.tsm_Kharidaran.Name = "tsm_Kharidaran";
            this.tsm_Kharidaran.Size = new System.Drawing.Size(192, 26);
            this.tsm_Kharidaran.Text = "خریداران پوست";
            this.tsm_Kharidaran.Click += new System.EventHandler(this.tsm_Kharidaran_Click);
            // 
            // tsm_Dampezeshk
            // 
            this.tsm_Dampezeshk.Name = "tsm_Dampezeshk";
            this.tsm_Dampezeshk.Size = new System.Drawing.Size(192, 26);
            this.tsm_Dampezeshk.Text = "دامپزشکان";
            this.tsm_Dampezeshk.Click += new System.EventHandler(this.tsm_Dampezeshk_Click);
            // 
            // tsm_GroupDam
            // 
            this.tsm_GroupDam.Name = "tsm_GroupDam";
            this.tsm_GroupDam.Size = new System.Drawing.Size(192, 26);
            this.tsm_GroupDam.Text = "گروه دام ";
            this.tsm_GroupDam.Click += new System.EventHandler(this.tsm_GroupDam_Click);
            // 
            // tsm_TypeDam
            // 
            this.tsm_TypeDam.Name = "tsm_TypeDam";
            this.tsm_TypeDam.Size = new System.Drawing.Size(192, 26);
            this.tsm_TypeDam.Text = "انواع دام";
            this.tsm_TypeDam.Click += new System.EventHandler(this.tsm_TypeDam_Click);
            // 
            // tsm_Bimari
            // 
            this.tsm_Bimari.Name = "tsm_Bimari";
            this.tsm_Bimari.Size = new System.Drawing.Size(192, 26);
            this.tsm_Bimari.Text = "انواع بیماری";
            this.tsm_Bimari.Click += new System.EventHandler(this.tsm_Bimari_Click);
            // 
            // tsm_TypeAlayesh
            // 
            this.tsm_TypeAlayesh.Name = "tsm_TypeAlayesh";
            this.tsm_TypeAlayesh.Size = new System.Drawing.Size(192, 26);
            this.tsm_TypeAlayesh.Text = "انواع آلایش";
            this.tsm_TypeAlayesh.Click += new System.EventHandler(this.tsm_TypeAlayesh_Click);
            // 
            // tsm_MosavabatAlayesh
            // 
            this.tsm_MosavabatAlayesh.Name = "tsm_MosavabatAlayesh";
            this.tsm_MosavabatAlayesh.Size = new System.Drawing.Size(192, 26);
            this.tsm_MosavabatAlayesh.Text = "مصوبات آلایش";
            this.tsm_MosavabatAlayesh.Click += new System.EventHandler(this.tsm_MosavabatAlayesh_Click);
            // 
            // tsm_Places
            // 
            this.tsm_Places.Name = "tsm_Places";
            this.tsm_Places.Size = new System.Drawing.Size(192, 26);
            this.tsm_Places.Text = "مکان‌های نگهدای لاشه";
            this.tsm_Places.Click += new System.EventHandler(this.tsm_Places_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(189, 6);
            // 
            // tsm_ANAlayesh
            // 
            this.tsm_ANAlayesh.Name = "tsm_ANAlayesh";
            this.tsm_ANAlayesh.Size = new System.Drawing.Size(192, 26);
            this.tsm_ANAlayesh.Text = "کسر و اضافات بهای آلایش";
            this.tsm_ANAlayesh.Click += new System.EventHandler(this.tsm_ANAlayesh_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_Entry,
            this.tsm_EntryLine,
            this.tsm_Tozin,
            this.tsm_MohasebeAlayesh,
            this.tsm_BedehiDamdar,
            this.tsm_BankPayment,
            this.tsm_SoratJalaseKharid});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(54, 26);
            this.toolStripDropDownButton2.Text = "برگه ها";
            // 
            // tsm_Entry
            // 
            this.tsm_Entry.Name = "tsm_Entry";
            this.tsm_Entry.Size = new System.Drawing.Size(189, 26);
            this.tsm_Entry.Text = "برگه ورود دام به مجتمع";
            // 
            // tsm_EntryLine
            // 
            this.tsm_EntryLine.Name = "tsm_EntryLine";
            this.tsm_EntryLine.Size = new System.Drawing.Size(189, 26);
            this.tsm_EntryLine.Text = "برگه ورود دام به خط کشتار";
            // 
            // tsm_Tozin
            // 
            this.tsm_Tozin.Name = "tsm_Tozin";
            this.tsm_Tozin.Size = new System.Drawing.Size(189, 26);
            this.tsm_Tozin.Text = "برگه توزین";
            // 
            // tsm_MohasebeAlayesh
            // 
            this.tsm_MohasebeAlayesh.Name = "tsm_MohasebeAlayesh";
            this.tsm_MohasebeAlayesh.Size = new System.Drawing.Size(189, 26);
            this.tsm_MohasebeAlayesh.Text = "برگه محاسبه الایش";
            // 
            // tsm_BedehiDamdar
            // 
            this.tsm_BedehiDamdar.Name = "tsm_BedehiDamdar";
            this.tsm_BedehiDamdar.Size = new System.Drawing.Size(189, 26);
            this.tsm_BedehiDamdar.Text = "برگه بدهی دامداران";
            // 
            // tsm_BankPayment
            // 
            this.tsm_BankPayment.Name = "tsm_BankPayment";
            this.tsm_BankPayment.Size = new System.Drawing.Size(189, 26);
            this.tsm_BankPayment.Text = "برگه لیست پرداخت بانک";
            // 
            // tsm_SoratJalaseKharid
            // 
            this.tsm_SoratJalaseKharid.Name = "tsm_SoratJalaseKharid";
            this.tsm_SoratJalaseKharid.Size = new System.Drawing.Size(189, 26);
            this.tsm_SoratJalaseKharid.Text = "برگه صورت جلسه خرید";
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_Report_Daily,
            this.tsm_Report_DetailsDaily,
            this.tsm_Report_GroupDaily,
            this.toolStripMenuItem7,
            this.tsm_Report_EntryDaily,
            this.tsm_Report_KolKoshtar,
            this.tsm_Report_HesabDamdar,
            this.tsm_Report_KharidAlayesh,
            this.tsm_Report_Kharidar,
            this.tsm_Report_SoratJalaseKharid,
            this.tsm_Report_DetailsSoratJalase,
            this.tsm_Report_Places});
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(59, 26);
            this.toolStripDropDownButton3.Text = "گزارشات";
            // 
            // tsm_Report_Daily
            // 
            this.tsm_Report_Daily.Name = "tsm_Report_Daily";
            this.tsm_Report_Daily.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_Daily.Text = "آمار روزانه کشتار";
            // 
            // tsm_Report_DetailsDaily
            // 
            this.tsm_Report_DetailsDaily.Name = "tsm_Report_DetailsDaily";
            this.tsm_Report_DetailsDaily.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_DetailsDaily.Text = "گزارش ریز کشتار";
            // 
            // tsm_Report_GroupDaily
            // 
            this.tsm_Report_GroupDaily.Name = "tsm_Report_GroupDaily";
            this.tsm_Report_GroupDaily.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_GroupDaily.Text = "آمار روزانه کشتار به تفکیک گروه";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(240, 6);
            // 
            // tsm_Report_EntryDaily
            // 
            this.tsm_Report_EntryDaily.Name = "tsm_Report_EntryDaily";
            this.tsm_Report_EntryDaily.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_EntryDaily.Text = "ورود روزانه دام";
            // 
            // tsm_Report_KolKoshtar
            // 
            this.tsm_Report_KolKoshtar.Name = "tsm_Report_KolKoshtar";
            this.tsm_Report_KolKoshtar.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_KolKoshtar.Text = "کل کشتار";
            // 
            // tsm_Report_HesabDamdar
            // 
            this.tsm_Report_HesabDamdar.Name = "tsm_Report_HesabDamdar";
            this.tsm_Report_HesabDamdar.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_HesabDamdar.Text = "کارت حساب دامدار";
            // 
            // tsm_Report_KharidAlayesh
            // 
            this.tsm_Report_KharidAlayesh.Name = "tsm_Report_KharidAlayesh";
            this.tsm_Report_KharidAlayesh.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_KharidAlayesh.Text = "گزارش خرید آلایش به تفکیک کد دامدار";
            // 
            // tsm_Report_Kharidar
            // 
            this.tsm_Report_Kharidar.Name = "tsm_Report_Kharidar";
            this.tsm_Report_Kharidar.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_Kharidar.Text = "گزارش خریدار پوست";
            // 
            // tsm_Report_SoratJalaseKharid
            // 
            this.tsm_Report_SoratJalaseKharid.Name = "tsm_Report_SoratJalaseKharid";
            this.tsm_Report_SoratJalaseKharid.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_SoratJalaseKharid.Text = "گزارش صورت جلسه خرید";
            // 
            // tsm_Report_DetailsSoratJalase
            // 
            this.tsm_Report_DetailsSoratJalase.Name = "tsm_Report_DetailsSoratJalase";
            this.tsm_Report_DetailsSoratJalase.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_DetailsSoratJalase.Text = "گزارش خلاصه صورت جلسه خرید";
            // 
            // tsm_Report_Places
            // 
            this.tsm_Report_Places.Name = "tsm_Report_Places";
            this.tsm_Report_Places.Size = new System.Drawing.Size(243, 26);
            this.tsm_Report_Places.Text = "گزارش ضبطی و سردخانه";
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_Setting_EntryFee,
            this.toolStripMenuItem8,
            this.tsm_Setting_Users});
            this.toolStripDropDownButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton4.Image")));
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(62, 26);
            this.toolStripDropDownButton4.Text = "تنظیمات";
            // 
            // tsm_Setting_EntryFee
            // 
            this.tsm_Setting_EntryFee.Name = "tsm_Setting_EntryFee";
            this.tsm_Setting_EntryFee.Size = new System.Drawing.Size(204, 26);
            this.tsm_Setting_EntryFee.Text = "مبلغ قبض ورودی به کشتارگاه";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(201, 6);
            // 
            // tsm_Setting_Users
            // 
            this.tsm_Setting_Users.Name = "tsm_Setting_Users";
            this.tsm_Setting_Users.Size = new System.Drawing.Size(204, 26);
            this.tsm_Setting_Users.Text = "لیست کاربران سیستم";
            // 
            // toolStripDropDownButton5
            // 
            this.toolStripDropDownButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_Utilities_Period,
            this.toolStripMenuItem9,
            this.tsm_Utilities_Backup,
            this.tsm_Utilities_Restore,
            this.toolStripMenuItem10,
            this.tsm_Utilities_Move,
            this.toolStripMenuItem11,
            this.tsm_Utilities_Calc,
            this.tsm_Utilities_About});
            this.toolStripDropDownButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton5.Image")));
            this.toolStripDropDownButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton5.Name = "toolStripDropDownButton5";
            this.toolStripDropDownButton5.Size = new System.Drawing.Size(59, 26);
            this.toolStripDropDownButton5.Text = "امکانات";
            // 
            // tsm_Utilities_Period
            // 
            this.tsm_Utilities_Period.Name = "tsm_Utilities_Period";
            this.tsm_Utilities_Period.Size = new System.Drawing.Size(192, 26);
            this.tsm_Utilities_Period.Text = "انتخابات دوره";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(189, 6);
            // 
            // tsm_Utilities_Backup
            // 
            this.tsm_Utilities_Backup.Name = "tsm_Utilities_Backup";
            this.tsm_Utilities_Backup.Size = new System.Drawing.Size(192, 26);
            this.tsm_Utilities_Backup.Text = "تهیه نسخه پشتیبان جزئی";
            // 
            // tsm_Utilities_Restore
            // 
            this.tsm_Utilities_Restore.Name = "tsm_Utilities_Restore";
            this.tsm_Utilities_Restore.Size = new System.Drawing.Size(192, 26);
            this.tsm_Utilities_Restore.Text = "بازیابی نسخه پشتیبان جزئی";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(189, 6);
            // 
            // tsm_Utilities_Move
            // 
            this.tsm_Utilities_Move.Name = "tsm_Utilities_Move";
            this.tsm_Utilities_Move.Size = new System.Drawing.Size(192, 26);
            this.tsm_Utilities_Move.Text = "انتقال اطلاعات";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(189, 6);
            // 
            // tsm_Utilities_Calc
            // 
            this.tsm_Utilities_Calc.Name = "tsm_Utilities_Calc";
            this.tsm_Utilities_Calc.Size = new System.Drawing.Size(192, 26);
            this.tsm_Utilities_Calc.Text = "ماشین حساب";
            // 
            // tsm_Utilities_About
            // 
            this.tsm_Utilities_About.Name = "tsm_Utilities_About";
            this.tsm_Utilities_About.Size = new System.Drawing.Size(192, 26);
            this.tsm_Utilities_About.Text = "درباره...";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(77, 26);
            this.toolStripButton1.Text = "خروج از سیستم";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.toolStrip2.Font = new System.Drawing.Font("B Homa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_Entry,
            this.btn_EntryLine,
            this.btn_Tozin,
            this.btn_MohasebeAlayesh,
            this.btn_CardDamdar,
            this.btn_Bedehi,
            this.btn_Exit});
            this.toolStrip2.Location = new System.Drawing.Point(0, 29);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(784, 45);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btn_Entry
            // 
            this.btn_Entry.Image = ((System.Drawing.Image)(resources.GetObject("btn_Entry.Image")));
            this.btn_Entry.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Entry.Name = "btn_Entry";
            this.btn_Entry.Size = new System.Drawing.Size(94, 42);
            this.btn_Entry.Text = "ورود دام به مجتمع";
            this.btn_Entry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btn_EntryLine
            // 
            this.btn_EntryLine.Image = ((System.Drawing.Image)(resources.GetObject("btn_EntryLine.Image")));
            this.btn_EntryLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EntryLine.Name = "btn_EntryLine";
            this.btn_EntryLine.Size = new System.Drawing.Size(102, 42);
            this.btn_EntryLine.Text = "ورود دام به خط کشتار";
            this.btn_EntryLine.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btn_Tozin
            // 
            this.btn_Tozin.Image = ((System.Drawing.Image)(resources.GetObject("btn_Tozin.Image")));
            this.btn_Tozin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Tozin.Name = "btn_Tozin";
            this.btn_Tozin.Size = new System.Drawing.Size(37, 42);
            this.btn_Tozin.Text = "توزین";
            this.btn_Tozin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btn_MohasebeAlayesh
            // 
            this.btn_MohasebeAlayesh.Image = ((System.Drawing.Image)(resources.GetObject("btn_MohasebeAlayesh.Image")));
            this.btn_MohasebeAlayesh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_MohasebeAlayesh.Name = "btn_MohasebeAlayesh";
            this.btn_MohasebeAlayesh.Size = new System.Drawing.Size(75, 42);
            this.btn_MohasebeAlayesh.Text = "محاسبه آلایش";
            this.btn_MohasebeAlayesh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btn_CardDamdar
            // 
            this.btn_CardDamdar.Image = ((System.Drawing.Image)(resources.GetObject("btn_CardDamdar.Image")));
            this.btn_CardDamdar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CardDamdar.Name = "btn_CardDamdar";
            this.btn_CardDamdar.Size = new System.Drawing.Size(94, 42);
            this.btn_CardDamdar.Text = "کارت حساب دامدار";
            this.btn_CardDamdar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btn_Bedehi
            // 
            this.btn_Bedehi.Image = ((System.Drawing.Image)(resources.GetObject("btn_Bedehi.Image")));
            this.btn_Bedehi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Bedehi.Name = "btn_Bedehi";
            this.btn_Bedehi.Size = new System.Drawing.Size(74, 42);
            this.btn_Bedehi.Text = "بدهی دامداران";
            this.btn_Bedehi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btn_Exit
            // 
            this.btn_Exit.Image = ((System.Drawing.Image)(resources.GetObject("btn_Exit.Image")));
            this.btn_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(34, 42);
            this.btn_Exit.Text = "خروج";
            this.btn_Exit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 77);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(784, 497);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("B Nazanin", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainMenu";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btn_Entry;
        private System.Windows.Forms.ToolStripButton btn_EntryLine;
        private System.Windows.Forms.ToolStripButton btn_Tozin;
        private System.Windows.Forms.ToolStripButton btn_MohasebeAlayesh;
        private System.Windows.Forms.ToolStripButton btn_CardDamdar;
        private System.Windows.Forms.ToolStripButton btn_Bedehi;
        private System.Windows.Forms.ToolStripButton btn_Exit;
        private System.Windows.Forms.ToolStripMenuItem tsm_TypeDamdar;
        private System.Windows.Forms.ToolStripMenuItem tsm_Damdar;
        private System.Windows.Forms.ToolStripMenuItem tsm_LineKoshtar;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem tsm_Kharidaran;
        private System.Windows.Forms.ToolStripMenuItem tsm_Dampezeshk;
        private System.Windows.Forms.ToolStripMenuItem tsm_GroupDam;
        private System.Windows.Forms.ToolStripMenuItem tsm_TypeDam;
        private System.Windows.Forms.ToolStripMenuItem tsm_Bimari;
        private System.Windows.Forms.ToolStripMenuItem tsm_TypeAlayesh;
        private System.Windows.Forms.ToolStripMenuItem tsm_MosavabatAlayesh;
        private System.Windows.Forms.ToolStripMenuItem tsm_Places;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem tsm_ANAlayesh;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem tsm_Entry;
        private System.Windows.Forms.ToolStripMenuItem tsm_EntryLine;
        private System.Windows.Forms.ToolStripMenuItem tsm_Tozin;
        private System.Windows.Forms.ToolStripMenuItem tsm_MohasebeAlayesh;
        private System.Windows.Forms.ToolStripMenuItem tsm_BedehiDamdar;
        private System.Windows.Forms.ToolStripMenuItem tsm_BankPayment;
        private System.Windows.Forms.ToolStripMenuItem tsm_SoratJalaseKharid;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_Daily;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_DetailsDaily;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_GroupDaily;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_EntryDaily;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_KolKoshtar;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_HesabDamdar;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_KharidAlayesh;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_Kharidar;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_SoratJalaseKharid;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_DetailsSoratJalase;
        private System.Windows.Forms.ToolStripMenuItem tsm_Report_Places;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton4;
        private System.Windows.Forms.ToolStripMenuItem tsm_Setting_EntryFee;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem tsm_Setting_Users;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton5;
        private System.Windows.Forms.ToolStripMenuItem tsm_Utilities_Period;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem tsm_Utilities_Backup;
        private System.Windows.Forms.ToolStripMenuItem tsm_Utilities_Restore;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem tsm_Utilities_Move;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem tsm_Utilities_Calc;
        private System.Windows.Forms.ToolStripMenuItem tsm_Utilities_About;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

