﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koshtargah.DataLayer.Repositories
{
    public interface IKoshtargahRepository
    {

        //جدول دامدار
        List<tbl_Damdar> GetAllDamdar();
        IEnumerable<tbl_Damdar> GetDamdarByFilter(string paramter);
        tbl_Damdar GetDamdarById(int code_Damdar);
        bool InsertDamdar(tbl_Damdar damdar);
        bool UpdateDamdar(tbl_Damdar damdar);
        bool DeleteDamdar(tbl_Damdar damdar);
        bool DeleteDamdar(int code_Damdar);
        //جدول بیماری
        List<tbl_Bimari> GetAllBimari();
        tbl_Bimari  GetBimariById(int code_Bimari);
        bool InsertBimari(tbl_Bimari bimari);
        bool UpdateBimari(tbl_Bimari bimari);
        bool DeleteBimari(tbl_Bimari bimari);
        bool DeleteBimari(int code_Bimari);
        //جدول دامپزشک
        List<tbl_Dampezeshk> GetAllDampezeshk();
        tbl_Dampezeshk GetDampezeshkById(int code_Dampezeshk);
        bool InsertDampezeshk(tbl_Dampezeshk dampezeshk);
        bool UpdateDampezeshk(tbl_Dampezeshk dampezeshk);
        bool DeleteDampezeshk(tbl_Dampezeshk dampezeshk);
        bool DeleteDampezeshk(int code_Dampezeshk);
        //جدول گروه دام
        List<tbl_GroupDam> GetAllGroupDam();
        tbl_GroupDam GetGroupDamById(int code_GroupDam);
        bool InsertGroupDam(tbl_GroupDam groupDam);
        bool UpdateGroupDam(tbl_GroupDam groupDam);
        bool DeleteGroupDam(tbl_GroupDam groupDam);
        bool DeleteGroupDam(int code_GroupDam);
        //جدول انواع دام
        List<tbl_TypeDam> GetAllTypeDam();
        tbl_TypeDam GetTypeDamById(int code_TypeDam);
        bool InsertTypeDam(tbl_TypeDam TypeDam);
        bool UpdateTypeDam(tbl_TypeDam TypeDam);
        bool DeleteTypeDam(tbl_TypeDam TypeDam);
        bool DeleteTypeDam(int code_TypeDam);
        //جدول خریداران
        List<tbl_Kharidaran> GetAllKharidar();
        tbl_Kharidaran GetKharidarById(int code_kharidar);
        bool InsertKharidar(tbl_Kharidaran kharidar);
        bool UpdateKharidar(tbl_Kharidaran kharidar);
        bool DeleteKharidar(tbl_Kharidaran kharidar);
        bool DeleteKharidar(int code_kharidar);
        //جدول خط کشتار
        List<tbl_LineKoshtar> GetAllLineKoshtar();
        tbl_LineKoshtar GetLineKoshtarById(int code_LineKoshtar);
        bool InsertLineKoshtar(tbl_LineKoshtar lineKoshtar);
        bool UpdateLineKoshtar(tbl_LineKoshtar lineKoshtar);
        bool DeleteLineKoshtar(tbl_LineKoshtar lineKoshtar);
        bool DeleteLineKoshtar(int code_LineKoshtar);
        //جدول مکان
        List<tbl_Places> GetAllPlaces();
        tbl_Places GetPlacesById(int code_Places);
        bool InsertPlaces(tbl_Places Places);
        bool UpdatePlaces(tbl_Places Places);
        bool DeletePlaces(tbl_Places Places);
        bool DeletePlaces(int code_Places);
        //جدول انواع آلایش
        //جدول انواع دامدار
        List<tbl_TypeDamdar> GetAllTypeDamdar();
        tbl_TypeDamdar GetTypeDamdarById(int code_TypeDamdar);
        bool InsertTypeDamdar(tbl_TypeDamdar TypeDamdar);
        bool UpdateTypeDamdar(tbl_TypeDamdar TypeDamdar);
        bool DeleteTypeDamdar(tbl_TypeDamdar TypeDamdar);
        bool DeleteTypeDamdar(int code_TypeDamdar);

    }
}
