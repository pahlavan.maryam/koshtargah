﻿using Koshtargah.DataLayer.Repositories;
using Koshtargah.DataLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koshtargah.DataLayer.Context
{
    public class UnitofWork:IDisposable
    {
        Koshtargah_DBEntities db = new Koshtargah_DBEntities();

        private IKoshtargahRepository _koshtargahRepository;
        public IKoshtargahRepository KoshtargahRepository {
            get
            {
                if(_koshtargahRepository == null)
                {
                    _koshtargahRepository = new KoshtargahRepository(db);
                }
                return _koshtargahRepository;
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }
        public void Save()
        {
            db.SaveChanges();
        }
    }
}
