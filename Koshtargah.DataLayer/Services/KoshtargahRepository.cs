﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Koshtargah.DataLayer.Repositories;


namespace Koshtargah.DataLayer.Services
{
    public class KoshtargahRepository : IKoshtargahRepository
    {
        private Koshtargah_DBEntities db;

        public KoshtargahRepository(Koshtargah_DBEntities context)
        {
            db = context;
        }

        public bool DeleteBimari(tbl_Bimari bimari)
        {
            try
            {
                db.Entry(bimari).State=EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBimari(int code_Bimari)
        {
            try
            {
                var bimariId = GetBimariById(code_Bimari);
                DeleteBimari(bimariId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDamdar(tbl_Damdar damdar)
        {
            try
            {
                db.Entry(damdar).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDamdar(int code_Damdar)
        {
            try
            {
                var damdarId = GetDamdarById(code_Damdar);
                DeleteDamdar(damdarId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDampezeshk(tbl_Dampezeshk dampezeshk)
        {
            try
            {
                db.Entry(dampezeshk).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDampezeshk(int code_Dampezeshk)
        {
            try
            {
                var dampezeshkId = GetDampezeshkById(code_Dampezeshk);
                DeleteDampezeshk(dampezeshkId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGroupDam(tbl_GroupDam groupDam)
        {
            try
            {
                db.Entry(groupDam).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGroupDam(int code_GroupDam)
        {
            try
            {
                var groupDamId = GetGroupDamById(code_GroupDam);
                DeleteGroupDam(groupDamId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteLineKoshtar(tbl_LineKoshtar lineKoshtar)
        {
            try
            {
                db.Entry(lineKoshtar).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteLineKoshtar(int code_LineKoshtar)
        {
            try
            {
                var LineKoshtarId = GetLineKoshtarById(code_LineKoshtar);
                DeleteLineKoshtar(LineKoshtarId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePlaces(tbl_Places Places)
        {
            try
            {
                db.Entry(Places).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePlaces(int code_Places)
        {
           try
            {
                var PlacesId = GetPlacesById(code_Places);
                DeletePlaces(PlacesId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteTypeDam(tbl_TypeDam TypeDam)
        {
            try
            {
                db.Entry(TypeDam).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteTypeDam(int code_TypeDam)
        {
            try
            {
                var typeDam = GetTypeDamById(code_TypeDam);
                DeleteTypeDam(typeDam);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteTypeDamdar(tbl_TypeDamdar TypeDamdar)
        {
            try
            {
                db.Entry(TypeDamdar).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteTypeDamdar(int code_TypeDamdar)
        {
            try
            {
                var TypeDamdarId = GetTypeDamdarById(code_TypeDamdar);
                DeleteTypeDamdar(TypeDamdarId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<tbl_Bimari> GetAllBimari()
        {
            return db.tbl_Bimari.ToList();
        }

        public List<tbl_Damdar> GetAllDamdar()
        {
            return db.tbl_Damdar.ToList();
        }

        public List<tbl_Dampezeshk> GetAllDampezeshk()
        {
            return db.tbl_Dampezeshk.ToList();
        }

        public List<tbl_GroupDam> GetAllGroupDam()
        {
            return db.tbl_GroupDam.ToList();
        }

        public List<tbl_LineKoshtar> GetAllLineKoshtar()
        {
            return db.tbl_LineKoshtar.ToList();
        }

        public List<tbl_Places> GetAllPlaces()
        {
            return db.tbl_Places.ToList();
        }

        public List<tbl_TypeDam> GetAllTypeDam()
        {
                return db.tbl_TypeDam.ToList();
        }

        public List<tbl_TypeDamdar> GetAllTypeDamdar()
        {
            return db.tbl_TypeDamdar.ToList();
        }

        public tbl_Bimari GetBimariById(int code_Bimari)
        {
            return db.tbl_Bimari.Find(code_Bimari);
        }

        public tbl_Damdar GetDamdarById(int code_Damdar)
        {
            return db.tbl_Damdar.Find(code_Damdar);
        }

        public tbl_Dampezeshk GetDampezeshkById(int code_Dampezeshk)
        {
            return db.tbl_Dampezeshk.Find(code_Dampezeshk);
        }

        public tbl_GroupDam GetGroupDamById(int code_GroupDam)
        {
            return db.tbl_GroupDam.Find(code_GroupDam);
        }

        public tbl_LineKoshtar GetLineKoshtarById(int code_LineKoshtar)
        {
            return db.tbl_LineKoshtar.Find(code_LineKoshtar);
        }

        public tbl_Places GetPlacesById(int code_Places)
        {
            return db.tbl_Places.Find(code_Places);
        }

        public tbl_TypeDam GetTypeDamById(int code_TypeDam)
        {
                return db.tbl_TypeDam.Find(code_TypeDam);

        }

        public tbl_TypeDamdar GetTypeDamdarById(int code_TypeDamdar)
        {
           return db.tbl_TypeDamdar.Find(code_TypeDamdar);
        }

        public bool InsertBimari(tbl_Bimari bimari)
        {
            try
            {
                db.tbl_Bimari.Add(bimari);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertDamdar(tbl_Damdar damdar)
        {
            try
            {
                db.tbl_Damdar.Add(damdar);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertDampezeshk(tbl_Dampezeshk dampezeshk)
        {
            try
            {
                db.tbl_Dampezeshk.Add(dampezeshk);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertGroupDam(tbl_GroupDam groupDam)
        {
            try
            {
                db.tbl_GroupDam.Add(groupDam);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertLineKoshtar(tbl_LineKoshtar lineKoshtar)
        {
            try
            {
                db.tbl_LineKoshtar.Add(lineKoshtar);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertPlaces(tbl_Places Places)
        {
            try
            {
                db.tbl_Places.Add(Places);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertTypeDam(tbl_TypeDam TypeDam)
        {
            try
            {
                db.tbl_TypeDam.Add(TypeDam);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertTypeDamdar(tbl_TypeDamdar TypeDamdar)
        {
            try
            {
                db.tbl_TypeDamdar.Add(TypeDamdar);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateBimari(tbl_Bimari bimari)
        {
            try
            {
                db.Entry(bimari).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateDamdar(tbl_Damdar damdar)
        {
            try
            {
                db.Entry(damdar).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateDampezeshk(tbl_Dampezeshk dampezeshk)
        {
            try
            {
                db.Entry(dampezeshk).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateGroupDam(tbl_GroupDam groupDam)
        {
            try
            {
                db.Entry(groupDam).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateLineKoshtar(tbl_LineKoshtar lineKoshtar)
        {
            try
            {
                db.Entry(lineKoshtar).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdatePlaces(tbl_Places Places)
        {
            try
            {
                db.Entry(Places).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateTypeDam(tbl_TypeDam TypeDam)
        {
            try
            {
                db.Entry(TypeDam).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateTypeDamdar(tbl_TypeDamdar TypeDamdar)
        {
            try
            {
                db.Entry(TypeDamdar).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<tbl_Kharidaran> GetAllKharidar()
        {
            return db.tbl_Kharidaran.ToList();
        }

        public tbl_Kharidaran GetKharidarById(int code_kharidar)
        {
            return db.tbl_Kharidaran.Find(code_kharidar);
        }

        public bool InsertKharidar(tbl_Kharidaran kharidar)
        {
            try
            {
                db.tbl_Kharidaran.Add(kharidar);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateKharidar(tbl_Kharidaran kharidar)
        {
            try
            {
                db.Entry(kharidar).State = EntityState.Modified;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteKharidar(tbl_Kharidaran kharidar)
        {
            try
            {
                db.Entry(kharidar).State = EntityState.Deleted;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteKharidar(int code_kharidar)
        {
            try
            {
                var kharidarId = GetKharidarById(code_kharidar);
                DeleteKharidar(kharidarId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<tbl_Damdar> GetDamdarByFilter(string paramter)
        {
            return db.tbl_Damdar.Where(c => c.Family.Contains(paramter) || c.Name.Contains(paramter)).ToList();
        }
    }
}
