﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Koshtargah.DataLayer
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Koshtargah_DBEntities : DbContext
    {
        public Koshtargah_DBEntities()
            : base("name=Koshtargah_DBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tbl_Bank> tbl_Bank { get; set; }
        public virtual DbSet<tbl_Bimari> tbl_Bimari { get; set; }
        public virtual DbSet<tbl_Damdar> tbl_Damdar { get; set; }
        public virtual DbSet<tbl_Dampezeshk> tbl_Dampezeshk { get; set; }
        public virtual DbSet<tbl_Kharidaran> tbl_Kharidaran { get; set; }
        public virtual DbSet<tbl_Places> tbl_Places { get; set; }
        public virtual DbSet<tbl_TypeAlayesh> tbl_TypeAlayesh { get; set; }
        public virtual DbSet<tbl_TypeDam> tbl_TypeDam { get; set; }
        public virtual DbSet<tbl_TypeDamdar> tbl_TypeDamdar { get; set; }
        public virtual DbSet<tbl_GroupDam> tbl_GroupDam { get; set; }
        public virtual DbSet<tbl_LineKoshtar> tbl_LineKoshtar { get; set; }
    }
}
